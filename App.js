import React, {useEffect, useState} from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {NavigationContainer} from '@react-navigation/native';

import {Beer, Wine, Tobacco, Drinks, Quantity, Credit, Login} from './navigation/Screens'
import {AsyncStorage} from "react-native";

const Stack = createStackNavigator();

const App = () => {
    const [value, setValue] = useState(null);
    useEffect(() => {
        // wrap your async call here
        const loadData = async () => {

            setValue(await AsyncStorage.getItem('login'));
            if (value !== null) {
                // We have data!!
                setValue(value);
                console.log(value);
            }
        };

        // then call it here
        loadData();
    }, []);
    if (value === '1') {
        return (

            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{
                        headerShown: false
                    }}
                    initialRouteName={"Beer"}
                >
                    <Stack.Screen name="Beer" component={Beer}/>
                    <Stack.Screen name="Wine" component={Wine}/>
                    <Stack.Screen name="Tobacco" component={Tobacco}/>
                    <Stack.Screen name="Drinks" component={Drinks}/>
                    <Stack.Screen name="Quantity" component={Quantity}/>
                    <Stack.Screen name="Credit" component={Credit}/>
                </Stack.Navigator>
            </NavigationContainer>
        )
    } else {
        return (

            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{
                        headerShown: false
                    }}
                    initialRouteName={"Login"}
                >
                    <Stack.Screen name="Login" component={Login}/>
                    <Stack.Screen name="Beer" component={Beer}/>
                    <Stack.Screen name="Wine" component={Wine}/>
                    <Stack.Screen name="Tobacco" component={Tobacco}/>
                    <Stack.Screen name="Drinks" component={Drinks}/>
                    <Stack.Screen name="Quantity" component={Quantity}/>
                    <Stack.Screen name="Credit" component={Credit}/>
                </Stack.Navigator>
            </NavigationContainer>
        )
    }

}


export default App
