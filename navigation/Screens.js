import React from "react"
import {

    createDrawerNavigator,
    createBottomTabNavigator,
    createAppContainer
} from "react-navigation";

import Beer from "../screens/Beer"
import Wine from "../screens/Wine"
import Tobacco from "../screens/Tobacco"
import Drinks from "../screens/drinks"
import Quantity from "../screens/Quantity"
import Credit from "../screens/MyCredit"
import Login from "../screens/login"


export {
    Login,
    Beer,
    Wine,
    Tobacco,
    Drinks,
    Quantity,
    Credit
}
