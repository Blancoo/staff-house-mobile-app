import React from "react"
import R, { useState } from "react";
import {
    ImageBackground,
    Platform,
    ScrollView,
    StyleSheet,
    Text, Modal,
    TextInput,
    TouchableOpacity,
    View,
    Image,
    Dimensions,
    SafeAreaView, AsyncStorage
} from "react-native";
import ButtonSpinner from 'react-native-button-spinner';
import { WebView } from 'react-native-webview';
import axios from "axios";
import { CheckBox } from 'react-native-elements'
import ToggleSwitch from 'toggle-switch-react-native'
import * as Icon from "react-native-vector-icons";
import { Input,Switch,Button} from "galio-framework";
const { width, height } = Dimensions.get("screen");



class Login extends React.Component {


    onPress = async () => {
        if (this.state.login === "adminstaff" && this.state.password === "adminstaff") {
            console.log("done")
            AsyncStorage.setItem(
                'login',
                '1',
            );

            this.props.navigation.navigate("Beer")
            const value = await AsyncStorage.getItem('login');
            if (value !== null) {
                // We have data!!
                console.log(value)

            }
        } else {
            console.log("error")
            this.setState({error: "Wrong password or login"})
        }
    }

    state={
        login: "",
        password:"",
        error:""
    }
    constructor()
    {
        super()
        this.state={
            show:false,
            login: "",
            password:"",
            error:""
        }
    }

    renderHeader() {

        return (
            <SafeAreaView>
                <ScrollView >
                    <View style={styles.headerContainer}>
                        <View style={styles.header}>
                            <View style={{ flex: 2, flexDirection: "row" }}>

                                <View style={styles.logo}>
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("Beer")}
                                    >
                                        <Image
                                            style={styles.tinyLogo}
                                            source={require('../assets/images/logo.png')}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
    renderStructure() {

        return (
            <SafeAreaView style={styles.container}>

                <ScrollView style={styles.container}>


                    <View style={styles.section}>
                        <View>

                            <Text style={styles.title}>Username</Text>
                        </View>
                        <Input
                            style={styles.Input}
                            placeholder = "Your Username"
                            placeholderTextColor = "#b7b7b7"
                            underlineColorAndroid = "white"
                            underlineColor="white"
                            autoCapitalize = "none"
                            textAlign="left"

                            onChangeText={(text) => this.setState({ login: text })}
                            value={this.state.login}

                        />

                    </View>
                    <View style={styles.section}>
                        <View>

                            <Text style={styles.title}>Password</Text>
                        </View>
                        <Input
                            style={styles.Input}
                            placeholder = "Your password"
                            placeholderTextColor = "#b7b7b7"
                            underlineColorAndroid = "white"
                            underlineColor="white"
                            autoCapitalize = "none"
                            textAlign="left"
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({ password: text })}
                            value={this.state.password}

                        />
                        <Text style={styles.titleerror}>{this.state.error} </Text>
                    </View>

                    <View style={styles.button}>
                        <Button style={styles.buybtn2 }     onPress={this.onPress}>
                            <Text  style={{ color:"white"}}>Login</Text>
                        </Button>

                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
    render() {

        return (

            <SafeAreaView style={styles.container}>
                {this.renderHeader()}
                <ScrollView style={styles.container}>
                    {this.renderStructure()}

                </ScrollView>
            </SafeAreaView>

        )
    }
}
export default Login;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    headerContainer: {
        top: 0,
        height: height * 0.15,
        width: width
    },
    titleerror: {
        fontSize: 12,
        marginVertical: 5,
        color:	"#FF0000"
    },
    model:{
        maxHeight:height*0.6,
        minWidth:width*0.9,

        backgroundColor:"#ffffff",
        margin:80,
        padding:20,
        borderRadius: 14,
        flex:1
    },
    header: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        height: height * 0.20,
        width: width,
        paddingHorizontal: 14
    },
    logo: {
        alignItems: "center",
        justifyContent: "center",
        flex: 2,
        paddingHorizontal: 16
    },
    price:{
        justifyContent: "space-between",
        flexDirection:"row"
    },
    priceResulatsContainer:{

        minWidth: '30%',
        borderBottomColor: "#48FF5A",
        borderBottomWidth: 1
    },
    priceResults:{
        textAlign: 'right',
        fontSize: 30,
        marginVertical: 14
    },
    button:{
        width:width,
    },
    buybtn:{
        width:width*0.9,
        backgroundColor:"#48FF5A",
    },
    buybtn2:{
        minHeight:45,
        maxHeight:46,
        borderColor:"#48FF5A",
        marginTop:30,
        alignItems: "center",
        justifyContent: "center",
        width:width*0.8,
        backgroundColor:"#48FF5A",
        alignItems: "center",
        justifyContent: "center",
    },
    buybtn3:{
        marginTop:30,
        alignItems: "center",
        justifyContent: "center",
        width:width*0.8,
        alignItems: "center",
        justifyContent: "center",
    },
    section: {

        flexDirection: "column",
        marginHorizontal: 14,
        marginBottom: 14,
        paddingBottom: 24,
        borderBottomColor: "#EAEAED",
        borderBottomWidth: 1
    },
    Input:{
        borderRadius: 14,
        borderWidth: 1,
        borderColor: "#48FF5A",
        height: 60,
        backgroundColor: '#FFFFFF'
    },
    title: {
        fontSize: 18,
        marginVertical: 14
    },
    groupsec: {
        flexDirection: "row",

        justifyContent: "space-between"
    },
    types: {
        borderRadius: 14,
        borderWidth: 1,
        borderColor: "#48FF5A",
    },
    group: {

        flexDirection: "row",
        borderRadius: 14,
        borderWidth: 1,
        borderColor: "#48FF5A",
        justifyContent: "space-between"

    },
    button: {
        flex: 1,
        padding: 14,
        alignContent: "center",
        alignItems: "center"
    },
    buttonText: {
        fontSize: 16,
        textAlign: "center",
        fontWeight: "700"
    },
    active: {
        color: "#FFF",
        backgroundColor: "#48FF5A"
    },
    activeText: {
        color: "#FFF"
    },
    first: {
        borderTopLeftRadius: 13,
        borderBottomLeftRadius: 13,
        borderTopRightRadius: 13,
        borderBottomRightRadius: 13
    },
    last: {
        borderTopRightRadius: 13,
        borderBottomRightRadius: 13
    },
    option: {
        marginBottom: 14,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    section1: {

        flexDirection: "column",
        marginHorizontal: 14,
        marginBottom: 14,
        paddingBottom: 24,

    },

});
